﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ExpressGenerator.UI.Menu
{
    public partial class Main : UserControl, IChangeable
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btnBegin_Click(object sender, RoutedEventArgs e)
        {
            Changer.Change(new BasicInfo());
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("See ya!", "Closing application..");
            Application.Current.Shutdown();
        }

        #region IChangeable

        public void Use(object state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
