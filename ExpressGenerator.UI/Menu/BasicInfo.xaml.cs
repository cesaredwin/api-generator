﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WF = System.Windows.Forms;
using ExpressGenerator.Core.Models;

namespace ExpressGenerator.UI.Menu
{
    public partial class BasicInfo : UserControl, IChangeable
    {
        public BasicInfo()
        {
            InitializeComponent();
            cbxSource.Populate(new[] { "BitBucket", "Github" });
        }

        public void ClearForm()
        {
            FormHelper.Clear(new List<TextBox>(){ txtApplicationName, txtDescription, txtAuthor, txtDatabase });
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
            Changer.Change(new Main());
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            Save();
            ClearForm();
            Changer.Change(new ConfigureObjects());
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new WF.FolderBrowserDialog();
            if(dialog.ShowDialog() == WF.DialogResult.OK)
            {
                var folder = dialog.SelectedPath;
                txtPath.Text = folder;
            }
        }

        #region Custom Methods

        // TODO: replace ugly work around with binding
        private void Save()
        {
            Application.Current.Properties["Author"] = new Author(txtAuthor.Text, null);
            Application.Current.Properties["Database"] = txtDatabase.Text;
            Application.Current.Properties["AppName"] = txtApplicationName.Text;
            Application.Current.Properties["AppDesc"] = txtDescription.Text;
            Application.Current.Properties["AppPath"] = txtPath.Text;
        }

        #endregion

        #region IChangeable

        public void Use(object state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
