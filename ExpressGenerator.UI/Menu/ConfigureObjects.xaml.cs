﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ExpressGenerator.Core.Models;

namespace ExpressGenerator.UI.Menu
{
    public partial class ConfigureObjects : UserControl, IChangeable
    {
        private Module module;
        private List<Module> modules = new List<Module>();

        public ConfigureObjects()
        {
            InitializeComponent();
            recycle();

            var existingModules = (List<Module>)Application.Current.Properties["Modules"];
            if (existingModules != null)
                lstModules.Populate(existingModules.Select(m => m.Name).ToArray());
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            module = new Module
            {
                Name = txtObjectName.Text,
                Properties = new Dictionary<string, string>()
            };
            modules.Add(module);
            lstModules.Populate(modules.Select(m => m.Name).ToArray());
            grdProperties.ItemsSource = null;

            var controls = new List<UIElement>() { txtObjectName, txtPropertyName, txtPropertyValue, btnAddProperty };
            controls.Enable();
            txtObjectName.Disable();
            btnCreate.Disable();
        }

        private void btnAddProperty_Click(object sender, RoutedEventArgs e)
        {
            module.Properties.Add(txtPropertyName.Text, txtPropertyValue.Text);
            grdProperties.Populate(module.Properties);

            FormHelper.AskAgain("Would you like to add another property?",
                () => txtPropertyName.Focus(),
                () => recycle());
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            if (modules.Any(m => m.Properties.Count() < 1))
                FormHelper.MissingData("All modules must have at least one property before continuing.");
            else
            {
                Application.Current.Properties["Modules"] = modules;
                Changer.Change(new Packages());
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            FormHelper.DataWarning("Are you sure you wish to exit? All data will be lost.",
                () => Changer.Change(new BasicInfo()));
        }

        private void lstModules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            recycle();
            var selectedModule = modules.Where(m => m.Name.Equals(lstModules.SelectedItem)).Select(m => m).FirstOrDefault();

            txtObjectName.Text = module.Name;
            txtPropertyName.Text = selectedModule.Properties.Keys.FirstOrDefault();
            txtPropertyValue.Text = selectedModule.Properties.Values.FirstOrDefault();
            grdProperties.Populate(selectedModule.Properties);
        }

        private void grdProperties_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            return;
        }

        #region Custom Methods

        private void recycle()
        {
            txtObjectName.Enable().Focus();
            btnCreate.Enable();
            grdProperties.ItemsSource = null;

            var controls = new List<UIElement>() { txtPropertyName, txtPropertyValue, btnAddProperty }.Disable();
            foreach (var c in controls.OfType<TextBox>())
                c.Clear();
        }

        #endregion

        #region IChangeable

        public void Use(object state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
