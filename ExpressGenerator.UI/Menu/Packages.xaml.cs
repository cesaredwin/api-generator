﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExpressGenerator.Core.Models;
using ExpressGenerator.Core.Builders;
using ExpressGenerator.Core;

namespace ExpressGenerator.UI.Menu
{
    public partial class Packages : UserControl, IChangeable
    {
        public Packages()
        {
            InitializeComponent();

            lstNpm.Populate(new[] { "express", "mongoose", "mongodb", "bower", "body-parser" });
            lstBower.Populate(new[] { "angular", "bootstrap", "jquery", "font-awesome" });
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Changer.Change(new ConfigureObjects());
        }

        private void btnContinue_Click(object sender, RoutedEventArgs e)
        {
            Changer.Change(new Main());
        }

        private void build()
        {
            // Get subprojects
            var subprojects = new List<SubProject>();
            var modules = (List<Module>)Application.Current.Properties["Modules"];
            modules.ForEach(m =>
            {
                var sub = new SubProject(m);
                subprojects.Add(sub);
            });

            // Get data from properties
            var author = getAppData<Author>("Author");
            var database = getAppData<String>("Database");
            var name = getAppData<string>("AppName");
            var desc = getAppData<string>("AppDesc");
            var path = getAppData<string>("AppPath");

            // Feed builder
            var builder = new ProjectBuilder(name, author.Name, desc, subprojects, database, path, null, getValues(lstNpm), null);
            var generator = new Generator();
            generator.Construct(builder);
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            build();
            MessageBox.Show("Project has been built!");
            Application.Current.Shutdown();
        }

        #region Custom Methods

        private T getAppData<T>(string propertyName)
        {
            return (T)Application.Current.Properties[propertyName];
        }

        private List<string> getValues(ListBox list)
        {
            var values = new List<string>();
            foreach(var item in list.SelectedItems)
            {
                values.Add(item.ToString());
            }
            return values;
        }

        #endregion

        #region IChangeable

        public void Use(object state)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}