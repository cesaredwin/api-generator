﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WF = System.Windows.Forms;

namespace ExpressGenerator.UI
{
    public static class FormHelper
    {
        public static TextBox Clear<T>(this T t) where T : TextBox
        {
            t.Text = string.Empty;
            return t;
        }

        public static void Clear<T>(this List<T> ts) where T : TextBox
        {
            ts.ForEach(t => t.Clear());
        }

        #region Disable and Enable

        public static UIElement Disable<T>(this T t) where T : UIElement
        {
            t.IsEnabled = false;
            return t;
        }

        public static List<UIElement> Disable<T>(this List<T> elements) where T : UIElement
        {
            elements.ForEach(e =>
            {
                e.IsEnabled = false;
            });
            return elements as List<UIElement>;
        }

        public static UIElement Enable<T>(this T t) where T : UIElement
        {
            t.IsEnabled = true;
            return t;
        }

        public static void Enable<T>(this List<T> elements) where T : UIElement
        {
            elements.ForEach(e => e.Enable());
        }

        #endregion

        #region Populate Item Source

        public static ItemsControl Populate<T, U, V>(this T list, Dictionary<U, V> items) where T : ItemsControl
        {
            list.ItemsSource = null;
            list.ItemsSource = items;
            return list;
        }

        public static ItemsControl Populate<T, U>(this T list, U[] items) where T : ItemsControl
        {
            list.ItemsSource = null;
            list.ItemsSource = items;
            return list;
        }

        #endregion

        #region Message Boxes

        public static void AskAgain(string msg, Action yesMethod, Action noMethod)
        {
            var dialog = WF.MessageBox.Show(msg, "Again?", WF.MessageBoxButtons.YesNo, WF.MessageBoxIcon.Question);
            if (dialog == WF.DialogResult.Yes) yesMethod();
            else noMethod();
        }

        public static void DataWarning(string msg, Action yesMethod, Action noMethod = null)
        {
            var dialog = WF.MessageBox.Show(msg, "Warning", WF.MessageBoxButtons.YesNo, WF.MessageBoxIcon.Warning);
            if (dialog == WF.DialogResult.Yes) yesMethod();
            else
            {
                if (noMethod == null) return;
                else noMethod();
            }
        }

        public static void MissingData(string msg)
        {
            var dialog = WF.MessageBox.Show(msg, "Error", WF.MessageBoxButtons.OK, WF.MessageBoxIcon.Error);
            if (dialog == WF.DialogResult.OK) return;
            else return;
        }

        public static void AskExit(string msg)
        {
            var dialog = WF.MessageBox.Show("Are you sure you want to exist?", "Application is closing..",
                                            WF.MessageBoxButtons.YesNo, WF.MessageBoxIcon.Question);
            if (dialog == WF.DialogResult.Yes) Application.Current.Shutdown();
            else return;
        }

        #endregion
    }
}
