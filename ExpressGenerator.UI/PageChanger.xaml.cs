﻿using System;
using System.Windows;
using System.Windows.Controls;
using ExpressGenerator.UI.Menu;

namespace ExpressGenerator.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PageChanger : Window
    {
        public PageChanger()
        {
            InitializeComponent();
            Changer.changer = this;
            Changer.Change(new Main());
        }
 
        public void Navigate(UserControl nextPage)
        {
            Content = nextPage;
        }
 
        public void Navigate(UserControl nextPage, object state)
        {
            Content = nextPage;
            IChangeable c = nextPage as IChangeable;
 
            if (c != null) c.Use(state);
            else throw new ArgumentException("Something went terribly wrong..");
        }
    }
}
