﻿namespace ExpressGenerator.UI
{
    public interface IChangeable
    {
        void Use(object state);
    }
}
