﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using ExpressGenerator.Core.Models;

namespace ExpressGenerator.UI.Models
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public void RaisePropertyChanged(string propertyName)
        {
            var handlers = PropertyChanged;

            handlers(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /*public class Module : ViewModel
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        private Dictionary<string, string> properties;
        public Dictionary<string, string> Properties
        {
            get { return properties; }
            set
            {
                properties = value;
                RaisePropertyChanged("Properties");
            }
        }
    }*/

    public static class Modules
    {
        static Modules()
        {
            new ObservableCollection<Module>();
        }
        public static ObservableCollection<Module> ModuleList { get; set; }
    }

    public class Packages : List<string> { }

    public class Author
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    public class Database
    {
        private string connection;
        public string Connection
        {
            get { return connection; }
            set { connection = value; }
        }
    }
}
