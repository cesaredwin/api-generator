﻿using System.Windows.Controls;

namespace ExpressGenerator.UI
{
    public static class Changer
    {
        public static PageChanger changer;

        public static void Change(UserControl newPage)
        {
            changer.Navigate(newPage);
        }

        public static void Change(UserControl newPage, object state)
        {
            changer.Navigate(newPage, state);
        }
    }
}
