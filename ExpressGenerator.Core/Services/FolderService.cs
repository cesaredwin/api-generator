﻿using System.IO;
using ExpressGenerator.Core.Models;

namespace ExpressGenerator.Core.Services
{   
    public class FolderService
    {
        public string GetPath<T>(T t) where T : Item => t.Path;

        public string[] GetDirectories<T>(T t) where T : Item => Directory.GetDirectories(t.Path);

        /// <summary>
        /// Create a folder using the value of a generic Item's path.
        /// </summary>
        /// <typeparam name="T">An type that is or inherits from class Item.</typeparam>
        /// <param name="t">A generic that meets type restriction.</param>
        public void CreateFolder<T>(T t) where T : Item
        {
            if (!Directory.Exists(t.Path))
                Directory.CreateDirectory(t.Path);
        }

        /// <summary>
        /// Creates a folder using the string parameter as the file path.
        /// </summary>
        /// <param name="path">Location that the folder will be created.</param>
        public void CreateFolder(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        /// <summary>
        /// Creates a subdirectory within the directory of a given Item path.
        /// </summary>
        /// <typeparam name="T">An type that is or inhreits from class Item.</typeparam>
        /// <param name="t">A generic that meets type restriction.</param>
        /// <param name="folderPath"></param>
        /// <returns>Returns a string path of the created subdirectory.</returns>
        public string CreateSubFolder<T>(T t, string folderPath) where T : Item
        {
            var rootDirectory = new DirectoryInfo(t.Path);
            return rootDirectory.CreateSubdirectory(folderPath).ToString();
        }

        /// <summary>
        /// Creates a subdirectory within the directory of a given Item path.
        /// </summary>
        /// <param name="root">Path before folder name</param>
        /// <param name="folderPath"></param>
        /// <returns>Returns a string path of created subdirectory.</returns>
        public string CreateSubFolder(string root, string folderPath)
        {
            var rootDirectory = new DirectoryInfo(root);
            return rootDirectory.CreateSubdirectory(folderPath).ToString();
        }

        /// <summary>
        /// Create file if it doesn't already exist, and write content to it.
        /// </summary>
        /// <param name="path">Path of the file to be created.</param>
        /// <param name="contents">Content that will be written to the file.</param>
        public void CreateFile(string path, string contents)
        {
            if (!File.Exists(path))
            {
                using (StreamWriter sw = new StreamWriter(path, true))
                {
                    sw.Write(contents);
                }
            }
        }

        /// <summary>
        /// Create a file if it doesn't already exist.
        /// </summary>
        /// <param name="file">Path of the file to be created.</param>
        public void CreateFile(string file)
        {
            if (!File.Exists(file)) File.Create(file);
        }

        /// <summary>
        /// Format a template with an array of string values.
        /// </summary>
        /// <param name="template">The empty template that will be formatted.</param>
        /// <param name="values">Values that will be brought into the template.</param>
        /// <returns>Returns a formatted string that includes the values from that array.</returns>
        public string Format(string template, string[] values) => string.Format(template, values);

        /// <summary>
        /// Format a template with a string value.
        /// </summary>
        /// <param name="template">The empty template that will be formatted.</param>
        /// <param name="value">Value that will be brought into the template.</param>
        /// <returns>Returns a formatted string that includes the string value.</returns>
        public string Format(string template, string value) => string.Format(template, value);
    }
}
