﻿using ExpressGenerator.Core.Models;
using ExpressGenerator.Core.Interfaces;

namespace ExpressGenerator.Core.Builders
{
    public abstract class Builder : IBuilder
    {
        protected Project project;

        public Project Project
        {
            get { return project; }
        }

        public abstract void BuildFolders();
        public abstract void BuildPrereqs();
        public abstract void BuildSubProjects();
        public abstract void BuildPackages();
    }
}
