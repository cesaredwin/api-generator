﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpressGenerator.Core.Models;
using ExpressGenerator.Core.Models.Managers;
using ExpressGenerator.Core.Helpers;
using ExpressGenerator.Core.Services;

namespace ExpressGenerator.Core.Builders
{
    public class ProjectBuilder : Builder
    {
        private FolderService fs => new FolderService();
        private string root => $@"{project.Path}\{project.Name}";

        public ProjectBuilder(string name, string authorName, string desc, List<SubProject> subs, string db, string path,
            string repo = null, List<string> npmPackages = null, List<string> bowerPackages = null)
        {
            project = new Project
            {
                Name = name,
                Creator = new Author(authorName, repo),
                Description = desc,
                SubProjects = subs,
                Database = new Database(db),
                Path = path,
                NpmPackages = npmPackages,
                BowerPackages = bowerPackages
            };
        }

        public override void BuildPrereqs()
        {
            fs.CreateFolder(root);
            Add("Server.js", ResourceHelper.Server);
            Add(".gitignore", ResourceHelper.GitIgnore);
            Add("README.md", fs.Format(ResourceHelper.ReadMe, new[] { project.Name, project.Creator.Name, project.Description }));
            Add(".bowerrc", ResourceHelper.Bowerrc);
        }
 
        public override void BuildFolders()
        {
            var backend = fs.CreateSubFolder(root, "api");
            var frontend = fs.CreateSubFolder(root, "public");
            var config = fs.CreateSubFolder(root, "config");
            var subs = new[] { "fonts", "images", "styles", "scripts", "lib" }.ToList();

            fs.CreateFile($@"{config}\db.js",
                fs.Format(ResourceHelper.Config, new[] { project.Database.Production, project.Database.Development, project.Database.Staging }));
            fs.CreateFile($@"{root}\package.json", JsonFiles().Item1);
            fs.CreateFile($@"{root}\bower.json", JsonFiles().Item2);
            subs.ForEach(s => fs.CreateFolder($@"{frontend}\{s}"));
        }

        public override void BuildSubProjects()
        {
            project.SubProjects.ForEach(s =>
            {
                var modelName = s.Module.Name;
                var subFolder = fs.CreateSubFolder($@"{root}\api", modelName);

                Add($@"api\{modelName}\{modelName}.js", getModel(s));
                Add($@"api\{modelName}\{modelName}Controller.js", getController(s.Module));
            });
        }

        public override void BuildPackages()
        {
            var npm = new Npm($@"{root}");
            project.NpmPackages.ForEach(p => npm.Install(p, false));

            // TODO: incorporate bower package installation
            //var bower = new Bower($@"{project.Path}\public");
            //project.Packages.ForEach(p => npm.Install(p, false));
        }

        // TODO: migrate primary logic to ProjectService
        #region Methods

        private Tuple<string, string> JsonFiles()
        {
            var fillers = new[] { project.Name, project.Description, project.Creator.Name };
            return Tuple.Create(fs.Format(ResourceHelper.NpmJson, fillers), fs.Format(ResourceHelper.NpmJson, fillers));
        }

        private void Add(string fileName, string fileContents)
        {
            fs.CreateFile($@"{root}\{fileName}", fileContents);
        }

        private string getModel(SubProject sub)
        {
            var properties = string.Empty;
            sub.Module.Properties.ToList().ForEach(p =>
            {
                if (!string.IsNullOrWhiteSpace(properties))
                    properties += "\n\t";

                if (!p.Value.Contains("{"))
                    if (p.Value.Contains(","))
                        properties += $"{p.Key}: {{ {p.Value} }},";
                    else
                        properties += $"{p.Key}: {p.Value},";
                else
                    properties += $"{p.Key}: [{{ {p.Value} }}],";
            });
            return fs.Format(ResourceHelper.Model, 
                new[] { sub.Module.Name, properties.Substring(0, properties.LastIndexOf(",")) });
        }

        private string getController(Module module)
        {
            var values = string.Empty;
            module.Properties.Keys.ToList().ForEach(k =>
            {
                if (!string.IsNullOrWhiteSpace(values))
                    values += "\n\t";

                values += $"{k.ToLower()}: req.body.{k.ToLower()},";
            });
            return fs.Format(ResourceHelper.Controller, 
                new[] { module.Name, module.Name.ToLower(), values.Substring(0, values.LastIndexOf(",")) });
        }

        #endregion
    }
}
