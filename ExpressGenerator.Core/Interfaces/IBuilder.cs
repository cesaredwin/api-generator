﻿using ExpressGenerator.Core.Models;

namespace ExpressGenerator.Core.Interfaces
{
    internal interface IBuilder
    {
        Project Project { get; }
        void BuildPrereqs();
        void BuildFolders();
        void BuildSubProjects();
    }
}
