﻿namespace ExpressGenerator.Core.Interfaces
{
    internal interface IManager
    {
        string GetArgs(string args);
        void Install(string package, bool dev);
        void Uninstall(string package);
    }
}
