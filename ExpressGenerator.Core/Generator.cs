﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpressGenerator.Core.Builders;

namespace ExpressGenerator.Core
{
    public class Generator
    {
        public void Construct(ProjectBuilder builder)
        {
            builder.BuildPrereqs();
            builder.BuildFolders();
            builder.BuildSubProjects();
            builder.BuildPackages();
        }
    }
}
