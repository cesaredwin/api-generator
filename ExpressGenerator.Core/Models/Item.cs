﻿namespace ExpressGenerator.Core.Models
{
    public class Item
    {
        public string Name { get; set; }
        public string Contents { get; set; }
        public string Path { get; set; }

        public Item() { }
    }
}
