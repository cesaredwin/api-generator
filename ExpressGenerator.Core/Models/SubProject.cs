﻿namespace ExpressGenerator.Core.Models
{
    public class SubProject : Item
    {
        public Module Module { get; set; }
        
        public SubProject(Module module)
        {
            Module = module;
            Path = $@"\{module.Name}";
        }
    }
}
