﻿namespace ExpressGenerator.Core.Models
{
    public class Author
    {
        public string Name { get; set; }
        public string Repository { get; set; }

        public Author(string name, string repo)
        {
            Name = name;
            Repository = repo;
        }
    }
}
