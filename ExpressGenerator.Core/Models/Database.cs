﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressGenerator.Core.Models
{
    public class Database
    {
        public string Production { get; set; }
        public string Staging { get; set; }
        public string Development { get; set; }

        public Database(string db)
        {
            Production = $"LIVE-{db}";
            Staging = $"STAGE-{db}";
            Development = $"DEV-{db}";
        }
    }
}
