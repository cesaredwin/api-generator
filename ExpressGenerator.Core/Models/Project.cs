﻿using System.Collections.Generic;

namespace ExpressGenerator.Core.Models
{
    public class Project : Item
    {
        public Author Creator { get; set; }
        public string Description { get; set; }
        public List<SubProject> SubProjects { get; set; }
        public Database Database { get; set; }
        public List<string> NpmPackages { get; set; }
        public List<string> BowerPackages { get; set; }
    }
}
