﻿using ExpressGenerator.Core.Interfaces;

namespace ExpressGenerator.Core.Models.Managers
{
    public class Npm : PackageManager, IManager
    {
        public string GetArgs(string args) => $"/c {Name} {args}";
        
        public Npm(string directory) : base(directory)
        {
            Name = "Npm";
        }

        public void Install(string package, bool dev)
        {
            Argument = dev ? GetArgs($"install {package} --dev --save") : GetArgs($"install {package} --save");
            Run();
        }

        public void Uninstall(string package)
        {
            Argument = GetArgs($"uninstall {package}");
            Run();
        }
    }
}
