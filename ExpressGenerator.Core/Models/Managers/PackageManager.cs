﻿using System.Diagnostics;

namespace ExpressGenerator.Core.Models.Managers
{
    public class PackageManager
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string Argument { get; set; }

        public ProcessStartInfo ProcessInfo
        {
            get { return info; }
        }

        public string File => "Cmd.exe";
        public ProcessWindowStyle Style => ProcessWindowStyle.Hidden;

        private ProcessStartInfo info => new ProcessStartInfo
        {
            WorkingDirectory = Path,
            Arguments = Argument,
            FileName = File,
            WindowStyle = Style
        };

        public PackageManager(string directory)
        {
            Path = directory;
        }

        public void Run()
        {
            Process.Start(ProcessInfo);
        }
    }
}
