﻿using System.Collections.Generic;

namespace ExpressGenerator.Core.Models
{
    public class Module
    {
        public string Name { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}
