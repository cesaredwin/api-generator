﻿using ExpressGenerator.Core.Properties;

namespace ExpressGenerator.Core.Helpers
{
    public static class ResourceHelper
    {
        #region Resource properties

        public static string Server => Resources.Server;
        public static string Config => Resources.Config;
        public static string Model => Resources.Model_Body;
        public static string Controller => Resources.Controller_Body;
        public static string NpmJson => Resources.Npm_Json;
        public static string BowerJson => Resources.Bower_Json;
        public static string ReadMe => Resources.Read_Me;
        public static string GitIgnore => Resources.GitIgnore;
        public static string Bowerrc => Resources.Bowerrc;

        #endregion
    }
}
